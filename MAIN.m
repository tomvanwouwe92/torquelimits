% This is the header file that calls the TorqueLimitClassifier function
addpath('HillModel')

% Create some dummy input data to test
% Specify the osim-model name
% This model has 23 dofs and 92 muscles.
% Alternatively, use gait2354_simbody.osim
modelName = 'gait2392_simbody.osim';
% Set up to evaluate left hip flexion, hip
% adduction, hip rotation, knee flexion and ankle plantairflexion torques.
DOF = {'hip_flexion_l' 'hip_adduction_l' 'hip_rotation_l' 'knee_angle_l' 'ankle_angle_l'};
size = 100;   % number of samples
% Create input values for the generalized coordinates, velocities and
% torques.
q = 0.3*rand(size,5) - 0.15*ones(size,5);   % Select some realistic value for q and qdot
q_dot = 1*rand(size,5) - 0.5*ones(size,5);
tau = [50*rand(size,2) - 25*ones(size,2),10*rand(size,1) - 5*ones(size,1), 150*rand(size,2) - 75*ones(size,2)];  % Select some realistic value for the torques
% These dummy values seem to create a nice mix of feasible and unfeasible
% torques

% Call the classifier, it returns a vector which indicates whether the
% chosen sample is feasible, activations that are needed and the values of
% the reserveactuators.
[Torque_Manageable,Activations,ReserveActuators,MusclesOI] = TorqueLimitClassifier(modelName,DOF,0*q,0*q_dot,tau);

% Note: In case of infeasible torque (indicated by 0) the activations
% should max out, i.e. reach 1 (at least for some muscles) and the reserve
% actuator torques are non-trivial.