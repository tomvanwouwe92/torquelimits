# README #



### What is this repository for? ###

* Based on the state of the (musculo)-skeletal system return whether a specified set of torques (one for each DOF) is feasible.
* We focus on the left lower limb and have 5 degrees of freedom: hip flexion - hip adduction - hip rotation - knee flexion - ankle flexion.
* Version


### How do I get set up? ###

1. Download and install OpenSim 3.3. Installation Guide: https://simtk-confluence.stanford.edu/display/OpenSim/Installation+Guide
2. Configure Matlab Scripting Environment: https://simtk-confluence.stanford.edu/display/OpenSim/Scripting+with+Matlab
  IMPORTANT: The system architecture of Matlab & OpenSim must match (install 64-bit OpenSim if you use 64-bit Matlab).
3. Clone Or Download this repository.


### General Info ###

* The MAIN file provides dummy input samples as an example for the kinematics and torques. 
* The function 'TorqueLimitClassifier' is called and returns for each sample whether it is a feasible combination.
* For each sample the function returns the activations and reserve actuators that require minimal muscular effort to achieve the desired torque.
* Note that for a feasible torque, the reserve actuators should be close to 0.


### Who do I talk to? ###
